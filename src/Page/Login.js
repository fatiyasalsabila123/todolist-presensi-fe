import axios from "axios";
import React, { useState } from "react";
// import { Form, InputGroup } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
// import "../style/Login.css"
import Logo from "../image/maxresdefault-removebg-preview.png"

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:4000/users/login",
        {
          email: email,
          password: password,
        }
      );
      //jika repon status 200/ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login Berhasil !!!",
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("id", data.data.users.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.users.role);
        history.push("/home");
        //Untuk mereload
        setTimeout(()=>{
          window.location.reload();
          }, 1500)
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showConfirmButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    // <div className="body">
    //   <div className='login'>
    //      <div className=" pt-3 pb-5 px-5 login1">
    //      <div className="avatar">
    //         <i className="fa fa-user"></i>
    //       </div>
    //     <h2 style={{marginTop:"20px"}}>Form Login</h2>
    //     <Form onSubmit={login} method="POST">
    //       <div className="mb-3 ">
    //         <Form.Label>
    //           <strong>Email</strong>
    //         </Form.Label>
    //         <InputGroup className="d-flex gap-3 box-login">
    //           <Form.Control
    //             placeholder="Email"
    //             type="text"
    //             value={email}
    //             onChange={(e) => setEmail(e.target.value)}
    //             required
    //           />
    //         </InputGroup>
    //       </div>
    //       <div className="mb-3">
    //         <Form.Label>
    //           <strong>Password</strong>
    //         </Form.Label>
    //         <InputGroup className="d-flex gap-3 box-login">
    //           <Form.Control
    //             placeholder="Password"
    //             type="password"
    //             value={password}
    //             onChange={(e) => setPassword(e.target.value)}
    //             required
    //           />
    //         </InputGroup>
    //       </div>
    //       <button variant="primary" type="submit" className="mx-1 buton btn-login">
    //         Login
    //       </button>
    //       <p className='bottom'>
    //         <a href="/"> register </a>
    //       </p>
    //     </Form>
    //   </div>
    //   </div>
    // </div>
    <div className="bg-[url('https://wallpapers.com/images/high/minimalistic-business-setup-dudslamn7oardprl.jpg')] bg-no-repeat bg-cover h-screen">
      <section className="">
        <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
          <div className="mr-10">
          <a
            href="/"
            className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
          >
            <img
              className=" h-8 mr-1"
              src={Logo}
              alt="logo"
            />
            Sign-In
          </a>
          </div>
          <div className="w-full rounded-lg shadow-black bg-opacity-100 shadow-2xl dark:border md:mt-0 sm:max-w-md xl:p-0  ">
            <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
              <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                Sign in to your account
              </h1>
              <form onSubmit={login} className="space-y-4 md:space-y-6" action="#">
                <div>
                  <label
                    for="email"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Your email
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    placeholder="name@company.com"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </div>
                <div>
                  <label
                    for="password"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                  >
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="••••••••"
                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </div>
                <div className="flex items-center justify-between">
                  <div className="flex items-start">
                    <div className="flex items-center h-5">
                        {/* <input
                          id="remember"
                          aria-describedby="remember"
                          type="checkbox"
                          className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
                          required=""
                        /> */}
                    </div>
                    {/* <div className="ml-3 text-sm">
                      <label
                        for="remember"
                        className="text-gray-500 dark:text-gray-300"
                      >
                        Remember me
                      </label>
                    </div> */}
                  </div>
                </div>
                <button
                  type="submit"
                  className="w-full text-white bg-blue-700 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
                >
                  Sign in
                </button>
                <p className="text-sm font-light text-black dark:text-gray-400">
                  Don’t have an account yet?{" "}
                  <a
                    href="/register"
                    className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                  >
                    Sign up
                  </a>
                </p>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
