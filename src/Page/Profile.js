import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Swal from "sweetalert2";
import Navbar from "../component/Navbar";

export default function Profile() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUserName] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nohp, setNoHp] = useState("");
  const [profile, setProfile] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [Photoprofile, setPhotoProfile] = useState({
    email: "",
    password: "",
    username: "",
    alamat: "",
    nohp: "",
    profile: null,
  });

  const getId = async () => {
    await axios
      .get("http://localhost:4000/users/" + localStorage.getItem("id"))
      .then((res) => {
        setPhotoProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getId();
  },[]);

  const putProfile = async (e) => {
    e.preventDefault();
    e.persist();

    const data = new FormData();
    data.append("file", profile);
    data.append("username", username);
    data.append("alamat", alamat);
    data.append("email", email);
    data.append("nohp", nohp);
    data.append("password", password);

    try {
      await axios.put(
        `http://localhost:4000/users/${localStorage.getItem("id")}`,
        data
      );
      Swal.fire({
        title: 'Apakah yakin profile akan di edit',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: 'Ya',
        denyButtonText: `tidak jadi`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          Swal.fire('Saved!', '', 'success')
        } else if (result.isDenied) {
          Swal.fire('Changes are not saved', '', 'info')
        }
      })
      setTimeout(()=>{
        window.location.reload();
        }, 1500)
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    axios
      .get("http://localhost:4000/users/" + localStorage.getItem("id"))
      .then((response) => {
        const profil = response.data.data;
        setProfile(profil.profile);
        setUserName(profil.username);
        setAlamat(profil.alamat);
        setEmail(profil.email);
        setNoHp(profil.nohp);
        setPassword(profil.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);
  return (
    <div className="">
      <Navbar />
      <form
        className="bg-[url('')] h-screen bg-cover bg-no-repeat"
      >
        <div className="relative left-[30%] md:left-[30%] top-[60%] md:top-[23%] w-[45%] md:w-[18%]">
          <p className="text-xl md:text-3xl font-bold rounded-2xl bg-[#0c4a6e] p-2 text-white  text-center">
            Profile Saya
          </p>
        </div>
        <div className="ml-[5%] md:ml-[17%] mt-[45%] md:mt-[7%] md:w-[67%] leading-10 items-center block md:flex font-bold w-[92%] ">
          <div className=" border-black border p-5 rounded-2xl md:-mr-5 bg-gray-300/30 shadow-black shadow-2xl md:h-[24rem] h-[46rem]">
            <p className="font-bold text-3xl h-12 md:mt-0 mt-[80%] ">
              {Photoprofile.username}
            </p>
            <hr className="w-[80%]" />
            <p>
              <i class="fas fa-envelope-open-text text-[#0c4a6e]"></i>{" "}
              {Photoprofile.email}
            </p>
            <p>
              <i class="fas fa-map-marked text-[#0c4a6e]"></i>{" "}
              {Photoprofile.alamat}
            </p>
            <p>
              <i class="fas fa-phone-alt text-[#0c4a6e]"></i>{" "}
              {Photoprofile.nohp}
            </p>
            {/* <p className="md:mt-0 mt-[10%] w-[140%] md:ml-0 -ml-[6%] sm:text-center md:text-left"> */}
            <p className="text-justify md:mt-0 mt-[10%]">
              Kelola informasi profil Anda untuk mengontrol, melindungi dan mengamankan akun
            </p>
          </div>
          <img
            className="bg-[url('')] md:shadow-black md:shadow-2xl border-black border md:h-[27rem] h-[12rem] md:rounded-2xl md:w-[38%] md:static absolute md:bottom-0 bottom-[28%] w-[60%] rounded-full md:left-0 left-[20%]"
            src={Photoprofile.profile}
            alt=""
          />
        </div>
        <button
          className="ml-[93%] md:ml-[35%] relative bottom-[14%] right-[13rem] bg-[#0c4a6e] text-white font-bold p-2 px-5 rounded-2xl" type="button"
          onClick={handleShow}
        >
          Edit
        </button>
      </form>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton style={{ backgroundColor: "#5F9DF7" }}>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body className="form">
          <Form onSubmit={putProfile}>
            <div className="mb-3">
              <Form.Label>
                <strong>Photo Profile</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="file"
                  // value={photoProfile}
                  onChange={(e) => setProfile(e.target.files[0])}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Username</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukan Username"
                  value={username}
                  onChange={(e) => setUserName(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Alamat</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="text"
                  placeholder="Masukan Alamat"
                  value={alamat}
                  onChange={(e) => setAlamat(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Email</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="email"
                  placeholder="Masukan Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>No Handphone</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="number"
                  placeholder="Masukan Nomor Handphone"
                  value={nohp}
                  onChange={(e) => setNoHp(e.target.value)}
                  required
                  className="number1"
                ></Form.Control>
              </InputGroup>
            </div>
            <div className="mb-3">
              <Form.Label>
                <strong>Password</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  type="password"
                  placeholder="*****"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                ></Form.Control>
              </InputGroup>
            </div>
            <Button
              variant="danger"
              className="mx-1 buton-btl bg-red-700"
              onClick={handleClose}
            >
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1 buton-btl bg-blue-700"
              onClick={handleClose}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
