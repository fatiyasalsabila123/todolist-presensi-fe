import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import Navbar from "../component/Navbar";
import PresensiPulang from "./PresensiPulang";

export default function Presensi() {
  const [nama, setNama] = useState("");
  const [tanggal, setTanggal] = useState("");
  const [waktu, setWaktu] = useState("");
  const [pages, setPages] = useState(0);
  const param = useParams();
  // const [presensi, setPresensi] = useState ({
  //   nama: "",
  //   gender: "",
  //   tanggal: "",
  // })

  const [list, setList] = useState([]);

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const history = useHistory();

  const absen = async (e) => {
    e.preventDefault();
    e.persist();

    // const formData = new FormData();
    // formData.append("tanggal", tanggal);
    // formData.append("waktu", waktu);

    try {
      await axios.post(
        "http://localhost:4000/absen_masuk", {
          tanggal,
          waktu,
          usersId: localStorage.getItem("id"),
        }
      );
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Berhasil Absensi Masuk',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async () => {
    await axios
      .get(
        `http://localhost:4000/absen_masuk?user_id=${localStorage.getItem("id")}`
      )
      .then((res) => {
        setList(res.data.data);
        // setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };


  useEffect(() => {
    getAll();
  }, []);

  return (
    <div className="h-[100rem] bg-gradient-to-b from-white to-blue-200 bg-no-repeat bg-cover font-bold">
      <Navbar />
      <p className="text-center mt-5 -mb-[8%] md:text-2xl md:ml-[40%] ml-[25%] font-bold bg-blue-700 text-white md:w-[16%] w-[50%] p-1 rounded-3xl pb-2">Presensi</p>
      <div className="ml-[48%] md:ml-[77%] mt-[10%]">
        <button
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-bold rounded-lg text-sm md:px-5 px-2 mt-5 md:mt-0 py-2 md:ml-[22%] ml-7 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          onClick={absen}
        >
          Absensi Masuk
        </button>
      </div>
      <div className="w-[80%] ml-[10%] mt-[2%] border-black ">

<div className="flex flex-col">
  <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
      <div className="overflow-hidden">
        <table className="min-w-full">
          <thead className="border bg-blue-700 rounded-lg ">
            <tr>
              <th scope="col" className="text-sm font-medium text-white px-6 py-2 text-left">
                No
              </th>
              <th scope="col" className="text-sm font-medium text-white px-6 py-2 text-left">
                Tanggal
              </th>
              <th scope="col" className="text-sm font-medium text-white px-6 py-2 text-left">
                Absen Masuk
              </th>
            </tr>
          </thead>
          <tbody className="">
          {/* {list.map((absenn, index) => ( */}
          {list.map((absenn, index) => (
              <tr className="border-black border hover:bg-gray-200" key={absenn.id}>
                {/* <td className="align-middle px-4 py-2">{index + 1}</td> */}
              <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-black">{index + 1}</td>
              <td className="text-sm text-black font-bold px-6 py-4 whitespace-nowrap">
                {absenn.tanggal}
              </td>
              <td className="text-sm text-black font-bold px-6 py-4 whitespace-nowrap">
                {absenn.waktu} WIB
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
      </div>
      <PresensiPulang/>

    
    </div>
  );
}
