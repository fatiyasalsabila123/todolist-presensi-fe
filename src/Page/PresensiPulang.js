import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import Swal from "sweetalert2";

export default function PresensiPulang() {
  const [tanggal, setTanggal] = useState("");
  const [waktu, setWaktu] = useState("");
  const [list, setList] = useState([]);
  const [hadir, setHadir] = useState("hadir");
  const [status, setStatus] = useState("");
  const [status1, setStatus1] = useState("");
  const [statusId, setStatusId] = useState(0);
  const [show, setShow] = useState(false);
  const [open, setOpen] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const absen = async (e) => {
    e.preventDefault();
    e.persist();

    // const formData = new FormData();
    // formData.append("tanggal", tanggal);
    // formData.append("waktu", waktu);

    try {
      await axios.post("http://localhost:4000/absen_pulang", {
        status,
        tanggal,
        waktu,
        usersId: localStorage.getItem("id"),
      });
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Berhasil Absensi Pulang',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const getAll = async () => {
    await axios
      .get(`http://localhost:4000/absen_pulang?users_id=${localStorage.getItem("id")}` )
      .then((res) => {
        setList(res.data.data);
        // setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  const getById = async (id) => {
    await axios
      .get("http://localhost:4000/absen_pulang/" + id)
      .then((res) => {
        setStatus1(res.data.data.status)
        setStatusId(res.data.data.id);
        console.log(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Keslahan" + error);
      });
  };

  useEffect(() => {
    getAll();
  }, []);

  const put = async (e) => {
    const data = {
      status: status1
    }
    try {
      await axios.put(`http://localhost:4000/absen_pulang/${statusId}`, data);
      Swal.fire({
        icon: "success",
        title: "Berhasil Di Edit !!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(()=>{
        window.location.reload();
        }, 1500)
    } catch (err) {
      console.log(err);
    }
    // Swal.fire({
    //   position: 'top-end',
    //   icon: 'success',
    //   title: 'Berhasil Absensi Masuk',
    //   showConfirmButton: false,
    //   timer: 1500
    // })
  };

  return (
    <div className="mb-[5%] -mt-10">
      <div className="ml-[48%] md:ml-[77%] mt-[10%]">
        <button
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-bold rounded-lg text-sm md:px-5 px-2 mt-5 md:mt-0 py-2 md:ml-[22%] ml-7 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          onClick={() => setShow(true)}
        >
          Absensi Pulang
        </button>
      </div>
      <div className="flex flex-col rounded-lg w-[80%] ml-[10%] mt-[2%] border-gray-200">
        <div className="overflow-x-auto sm:-mx-6 lg:-mx-8" onSubmit={put}>
          <div className="inline-block min-w-full sm:px-6 lg:px-8">
            <div className="overflow-hidden">
              <table className="min-w-full">
                <thead className="border-b bg-blue-700">
                  <tr >
                    <th
                      scope="col"
                      className="text-sm font-bold text-white px-6 py-2 text-left"
                    >
                      No
                    </th>
                    <th
                      scope="col"
                      className="text-sm font-bold text-white px-6 py-2 text-left"
                    >
                      Tanggal
                    </th>
                    <th
                      scope="col"
                      className="text-sm font-bold text-white px-6 py-2 text-left"
                    >
                      Absen Pulang
                    </th>
                    <th
                      scope="col"
                      className="text-sm font-bold text-white px-6 py-2 text-left"
                    >
                      Status
                    </th>
                    <th
                      scope="col"
                      className="text-sm font-bold text-white px-6 py-2 text-left"
                    >
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {/* {list.map((absenn, index) => ( */}
                  {list.map((absenn, index) => (
                    <tr className="border-b hover:bg-gray-200 " key={absenn.id}>
                      {/* <td classNameName="align-middle px-4 py-2">{index + 1}</td> */}
                      <td className="px-6 py-4 whitespace-nowrap text-sm font-bold text-gray-900 ">
                        {index + 1}
                      </td>
                      <td className="text-sm text-gray-900 font-bold px-6 py-4 whitespace-nowrap">
                        {absenn.tanggal}
                      </td>
                      <td className="text-sm text-gray-900 font-bold px-6 py-4 whitespace-nowrap">
                        {absenn.waktu} WIB
                      </td>
                      <td className="text-sm text-gray-900 font-bold px-6 py-4 whitespace-nowrap">
                        {absenn.status}
                      </td>
                      <td className="action text-2xl text-green-900 font-light px-6 md:py-4 whitespace-nowrap">
                        <button
                          type="button"
                          className="simpan"
                          onClick={() => {
                            setOpen(true);
                            getById(absenn.id);
                          }}
                        >
                          <i className="fas fa-edit"></i>
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <Modal show={open} onHide={!open}>
        <Modal.Header closeButton className="bg-blue-800 text-white">
          <Modal.Title>Edit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={put}>
            <div className="mb-3">
              <Form.Label>
                <strong>Status</strong>
              </Form.Label>
              <InputGroup className="flex gap-3">
                <Form.Control
                  placeholder="Masukan Status"
                  type="text"
                  required
                  value={status1}
                  onChange={(e) => setStatus1(e.target.value)}
                ></Form.Control>
              </InputGroup>
            </div>
            <Button
              variant=""
              className=" bg-red-600 text-white hover:bg-red-600 "
              onClick={() => {
                setOpen(false);
              }}
            >
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1  bg-blue-700 text-white"
              onClick={() => {
                setOpen(false);
              }}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton className="bg-blue-800 text-white">
          <Modal.Title>Absensi Pulang</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={absen}>
            <div className="mb-3">
              <Form.Label>
                <strong>Status</strong>
              </Form.Label>
              <InputGroup className="flex gap-3">
                <Form.Control
                  placeholder="Masukan Status"
                  type="text"
                  required
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                ></Form.Control>
              </InputGroup>
            </div>
            <Button variant="" className=" bg-red-700 text-white hover:bg-red-400" onClick={handleClose}>
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1 bg-blue-700 text-white"
              onClick={handleClose}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
