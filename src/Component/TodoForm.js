import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function TodoForm ({input, setInput, todos, setTodos, editTodo, SetEditTodos}) {
  const param = useParams();
  // const [note, setNote] = useState("");

  const updateTodo = async (title, id, completed) => {
    const formData = new FormData();
        formData.append("note",input);
  
      await Swal.fire({
        title: "Apakah Anda Yakin Untuk Di Edit?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes!",
      })
        .then((result) => {
          if (result.isConfirmed) {
            axios
              //mengedit data
             .put("http://localhost:4000/todolist/" + param.id, formData , )
          }
          
        })
        .catch((error) => {
          alert("Terjadi Kesalahan " + error);
          console.log(error);
        });
}

  useEffect(() => {
    if(editTodo) {
    }else {
      setInput ("");
    }
  },[setInput, editTodo])

  const onInputChange = (event) => {
    setInput(event.target.value);
  }

  const onFormSubmit = async(event) => {

    event.preventDefault();
    if(!editTodo) {
      try {
      await axios.post("http://localhost:4000/todolist", {
        note: input,
        usersId: localStorage.getItem("id"),
      });
      Swal.fire({
        icon: "success",
        title: "List Berhasil Di Tambahkan!!!",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(()=>{
        window.location.reload();
        }, 1500)
      } catch (error) {
        console.log(error);
      }
    } else {
      updateTodo(input, editTodo.id, editTodo.completed)
    }
  }
  
  return (  
    <div>
      <form onSubmit={onFormSubmit} className="flex mb-10 mt-[20%] md:mb-5">
        <input type="text" placeholder='Enter a Todo...' className='task-input outline-none w-[70%] p-2 mr-1 md:mr-6 text-xl text-black border border-black rounded-xl md:w-[45%] md:-ml-[20%] -ml-[89%] shadow-xl' value={input} onChange={onInputChange}/> 
        <button className='button-add w-[20%] md:w-[10%] text-xl rounded-xl cursor-pointer bg-blue-500 text-white' type='submit'>
        {editTodo ? "OK" : "Add"}
        </button>
      </form>
    </div>
  )
}
