import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, InputGroup, Modal } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Swal from "sweetalert2";

export default function ({ todos, setTodos, setEditTodo }) {
  const param = useParams();
  const [note, setNote] = useState("");
  // const [note1, setNote1] = useState("");
  const [noteId, setNoteId] = useState(0);
  const [show, setShow] = useState(false);
  const [open, setOpen] = useState(false);
  const [tanggal, setTanggal] = useState("");
  const [waktu, setaWaktu] = useState ("");

 

  const deleteAbsen = async (id) => {
    Swal.fire({
      title: " apakah yakin data mau di delete?",
      text: "Data Akan Di Hapus",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:4000/todolist/" + id);
        Swal.fire("Deleted!", "absensi Berhasil Di Hapus", "success");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    });
  };


  const putList = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
      formData.append("note", note)

    try {
      await axios.put(
        `http://localhost:4000/todolist/${noteId}`,
        formData
      );
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Berhasil Mengedit',
        showConfirmButton: false,
        timer: 1500
      })
      console.log(note);
      setTimeout(()=>{
        window.location.reload();
        }, 1500)
    } catch (err) {
      console.log(err);
    }
  };

  const getById = async (id) => {
    await axios
      .get("http://localhost:4000/todolist/" + id)
      .then((res) => {
        setNote(res.data.data.note);
        setNoteId(res.data.data.id);
        console.log(res.data.data); 
      })
      .catch((error) => {
        alert("Terjadi Keslahan" + error);
      });
  };

  const checklist = async (id) => {
 

    try {
      await axios.put("http://localhost:4000/todolist/selesai/" + id)
        window.location.reload();

    } catch (error) {
      console.log(error);
    }
  }

  return (
    <div className="mb-[5%] ">
      {todos.map((todo) => (
        <li
          className="list-none md:flex block -ml-[100%] md:-ml-[35%] border mr-10 mt-3 md:-mt-3 w-[115%]  p-2 max-h-[15%] md:max-h-[6%] rounded-2xl md:w-[90%] border-black shadow-xl bg-sky-200 overflow-y-auto"
          key={todo.id}
        >

          <p className="font-bold text-green-700">{todo.check}</p>
          <p
            className={`list mr-1 mt-12 md:mt-10  text-black ${
              todo.completed ? "complete" : ""
            } overflow-x-auto scrol`}
          >
            {todo.note}
          </p>

          <div className="ml-[5%] md:ml-[30%] mt-10 md:mt-10">
            <button
              className="button-complete task-button border-none outline-none bg-transparent text-xs rounded-full text-red-600 mr-2 md:text-2xl"
              onClick={() => checklist(todo.id)}
            >
              <i className="fa fa-check-circle  border-none outline-none bg-transparent text-2xl rounded-full text-yellow-600 mr-2"></i>
            </button>
            {/* <p>{todo.waktu}</p>
            <button
              className="button-complete task-button border-none outline-none bg-transparent text-xs rounded-full text-red-600 mr-2 md:text-2xl"
              onClick={checklist} type="submit"
            >
              <i className="fa fa-check-circle  border-none outline-none bg-transparent text-2xl rounded-full text-yellow-600 mr-2"></i>
            </button> */}
            <button
              className="button-edit task-button"
              type="button"
              onClick={() => {
                setOpen(true);
                getById(todo.id);
              }}
            >
              <i className="fa fa-edit  border-none outline-none bg-transparent text-2xl rounded-full text-blue-600 mr-2"></i>
            </button>
            <button
              className="button-delete task-button text-red-700  border-none outline-none bg-transparent text-2xl rounded-full  mr-2"
              onClick={() => deleteAbsen(todo.id)}
            >
              <i className="fa fa-trash"></i>
            </button>
          </div>
        </li>
      ))}

      <Modal show={open} onHide={!open}>
        <Modal.Header closeButton>
          <Modal.Title>Edit</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={putList}>
            <div className="mb-3">
              <Form.Label>
                <strong>Status</strong>
              </Form.Label>
              <InputGroup className="flex gap-3">
                <Form.Control
                  placeholder="Masukan Status"
                  type="text"
                  required
                  value={note}
                  onChange={(e) => setNote(e.target.value)}
                ></Form.Control>
              </InputGroup>
            </div>
            <Button variant="" className="mx-1" onClick={() => setOpen(false)}>
              Close
            </Button>
            <Button
              type="submit"
              className="mx-1 text-black"
              onClick={() => setOpen(false)}
            >
              Save
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
