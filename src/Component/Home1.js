import React from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";

export default function Home1() {
  return (
    <div className="">
      <Navbar />
      <div className="lg:flex md:block sm:text-sm ">
        <div className="">
          <div className="bg-transparent rounded-xl shadow-2xl shadow-black border-black opacity-1 mt-10 md:mb-[5%] md:mt-16 w-[80%] h-[80vh] lg:flex justify-around ml-[10%] md:ml-9 md:m-[3%] gap-[10%] sm:w-[95%] mb-[10%] bg-gradient-to-r from-sky-600 via-gray-300 to-indigo-500">
            <img
              className=" h-[50%] rounded-xl w-[100%] md:w-[46%] sm:h-[33rem]"
              src="https://i0.wp.com/warunkonline.com/wp-content/uploads/2022/08/videobuilder-15-1024x1024-min.png"
              alt=""
            />
            <div className="mt-[6%] ">
              <p className="w-[100%] md:text-4xl text-lg md:ml-0 ml-[5%] md:w-[50%] -mt-10 font-bold text-black">
                Web Presensi and To Do List
              </p>
              <p className="w-[90%] md:text-lg text-xs md:ml-[1%] ml-[4%] font-bold mt-4">
                {" "}
                To Do List merupakan daftar tugas atau hal yang ingin kamu
                kerjakan agar bisa diselesaikan dalam waktu tertentu.
              </p>
              <div className="mt-[2%] flex gap-2 md:gap-3">
                <div className="w-[40%] border border-black h-32 md:h-[40%] p-1 pb-5 md:p-2 md:ml-0 ml-6 rounded-md hover:shadow-xl hover:shadow-gray-700">
                    <a href="/presensi"><img className="h-[3.5rem] w-[20rem] md:h-[9rem] rounded-md"
                      src="https://images.squarespace-cdn.com/content/v1/5fa8cf337228f12e5c896583/1634879734054-IMTH4BUNVCVUFR2A2QO9/animasi+2D.jpg?format=1500w"
                      alt=""
                    />
                    </a>
                  <hr />
                  <p className="mt-2 font-bold text-xs text-center md:text-2xl">
                    {" "}
                    Presensi
                  </p>
                  <i className="fas fa-list-ul ml-10 mt-2 mb:mt-3 text-base md:text-xl md:ml-32"></i>
                </div>
                
                <div className="w-[40%] border border-black h-32 md:h-[17rem] rounded-md p-1 pb-5 md:p-2 md:ml-0 ml-3 hover:shadow-xl hover:shadow-gray-700">
                    <a href="/todolist"><img className="rounded-md"
                      src="https://startinfinity.s3.us-east-2.amazonaws.com/production/blog/post/27/main/hxz0RhMzDeHogUZK0JNA82JabI3IstKUUbYB3dD8.png"
                      alt=""
                    />
                    </a>
                  <hr />
                  <p className="mt-1 md:mt-3 font-bold text-xs  text-center md:text-2xl">
                    To Do List
                  </p>
                  <div>
                    
                  <i className="fas fa-calendar-alt ml-10 mt-2 mb:mt-4 text-base md:text-xl md:ml-32"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
  );
}
