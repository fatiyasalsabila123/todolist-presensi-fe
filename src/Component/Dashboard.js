import React, { useEffect, useState } from "react";
import Header from "./Header";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
import "../style/Home.css";
import Navbar from "./Navbar";
import axios from "axios";
import "../style/Dashboard.css"

export default function Dashboard() {
  const [input, setInput] = useState("");
  const [todos, setTodos] = useState([]);
  const [note, setNote] = useState("");
  const [editTodo, setEditTodo] = useState(null);
  const [pages, setPages] = useState(0);
  const [profile, setProfile] = useState("");
  const [username, setUserName] = useState("");
  const [Photoprofile, setPhotoProfile] = useState ({
    username: "",
    profile: null,
  })

  // const param = useParams();

  const getAll = async () => {
    await axios
      .get(
        `http://localhost:4000/todolist?user_id=${localStorage.getItem("id")}`
      )
      .then((res) => {
        setTodos(res.data.data);
        // setPages(res.data.data.totalPages);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  useEffect(() => {
    getAll();
    getId();
  }, []);

  const getId = async () => {
    await axios
      .get("http://localhost:4000/users/" + localStorage.getItem("id"))
      .then((res) => {
        setPhotoProfile(res.data.data);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan" + error);
      });
  };

  //  useEffect(() => {
  //   axios
  //     //get untuk menampilkan data
  //     .get("http://localhost:4000/todolist/" + param.id)
  //     .then((response) => {
  //       const todolist = response.data.data;
  //       setEditTodo(todolist.note);
  //     })
  //     .catch((error) => {
  //       alert("Terjadi Kesalahan Sir!" + error);
  //     });
  // }, []);

  return (
    <div className=" h-[90rem] ">
      <Navbar />
      <div className="justify-center block md:flex">
        <div className="mt-12 h-[] md:h-[39.5rem] bg-sky-200 p-4 md:rounded-l-xl">
          <div className="mt-[10rem]">
          <div className="flex gap-4 md:mt-0 -mt-[10rem] md:ml-0 ml-6">
            <img
              className="w-[5rem] rounded-full border-[#0c4a6e] border-2 h-[5rem]"
              src={Photoprofile.profile}
              alt=""
            />
            <div>
              <p className="font-bold text-base">To Do List</p>
              <p className="font-bold text-xl ">{Photoprofile.username}</p>
            </div>
          </div>
          <div className="block mt-5 md:mt-16">
            <div className="flex md:ml-0 ml-16">
              <i class="fas fa-list-alt mt-1"></i>
              <p className="ml-6 text-base font-semibold ">List tugas harian</p>
            </div>
            <div className="md:block flex gap-3 md:gap-0 ml-7 md:ml-16 mt-2">
              <span className="block mt-2">
                <i className="fa fa-check-circle"></i> Check list{" "}
              </span>
              <span className="block mt-2">
                <i className="fa fa-edit"></i> Edit
              </span>
              <span className="block mt-2">
                <i className="fa fa-trash"></i> Hapus
              </span>
            </div>
          </div>
          </div>
        </div>
        <div className=" md:border border-red pb-[60%] md:pb-[10%] mb-5 md:w-[50%] w-[100%] md:rounded-r-xl md:mt-12">
          <p className="text-center mt-5 text-3xl md:mb-0 -mb-[5%] font-bold  md:text-4xl">
            To Do List
          </p>

          <div className="block h-[60vh] md:flex justify-center items-center overflow-y-auto overflow-x-hidden kumpulan">
            <div   className="app-wrapper bg-transparent relative ml-[75%] min-w-[100%] min-h-[135%] p-8">
              <div className="sticky top-10 bottom-14 ">
                <TodoForm
                  input={input}
                  setInput={setInput}
                  todos={todos}
                  setTodos={setTodos}
                  editTodo={editTodo}
                  setEditTodo={setEditTodo}
                />
              </div>
              <div>
                <TodoList
                  todos={todos}
                  setTodos={setTodos}
                  setEditTodo={setEditTodo}
                />
              </div>
            </div>
          </div>
          {/* <div style={{border:"1px solid black"}}>

      <Register/>
      </div> */}
          {/* <ReactPaginate
          previousLabel={"<<"}
          nextLabel={">>"}
          breakLabel={"..."}
          pageCount={pages}
          marginPagesDisplayed={2}
          pageRangeDisplayed={3}
          onPageChange={(e) => getAll(e.selected)}
          containerClassName={"pagination justify-content-center"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          activeClassName={"active"}
        /> */}
        </div>
      </div>
    </div>
  );
}
