import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Form } from 'react-bootstrap';
import { useHistory, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Edit() {

    const [nama, setNama] = useState("");
    const [tanggal, setTanggal] = useState("");
    const [gender, setgender] = useState("");

    const history = useHistory();
    const param = useParams();

    // const putAbsen = async (e) => {
    //     e.presenDefault();
    //     e.persist();
    
    //     const formData = new FormData();
    //     formData.append("nama", nama);
    //     formData.append("gender", gender);
    //     formData.append("tanggal", tanggal);
    
    //     try {
    //       await axios.put(
    //         `http://localhost:4000/presensi/${localStorage.getItem("id")}`,
    //         formData
    //       );
    //       window.location.reload();
    //     } catch (err) {
    //       console.log(err);
    //     }
    //   };
    
    //   useEffect(() => {
    //      axios
    //       .get("http://localhost:4000/presensi/" + localStorage.getItem("id"))
    //       .then((response) => {
    //         const presensi = response.data.data;
    //         setNama(presensi.nama);
    //         setgender(presensi.gender);
    //         setTanggal(presensi.tanggal);
    //       })
    //       .catch((error) => {
    //         alert("Terjasi Kesalahan" + error);
    //       });
    //   }, []);

    useEffect(() => {
        axios
          //get untuk menampilkan data
          .get("http://localhost:4000/presensi/" + param.id)
          .then((response) => {
            const presensi = response.data.data;
            setNama(presensi.nama);
            setgender(presensi.gender);
            setTanggal(presensi.tanggal);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan Sir!" + error);
          });
      }, []);
    
      const putAbsen = async (e) => {
        e.preventDefault();
        e.persist();
    
        const formData = new FormData();
          formData.append("nama", nama);
          formData.append("gender", gender);
          formData.append("tanggal", tanggal);
    
        await Swal.fire({
          title: "Apakah Anda Yakin Untuk Di Edit?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes!",
        })
          .then((result) => {
            if (result.isConfirmed) {
              axios
                //mengedit data
               .put("http://localhost:4000/presensi/" + param.id, formData , )
            }
            
          })
          .then(() => {
            Swal.fire({
              title: "Berhasil",
              icon: "success",
              showConfirmButton : false,
              timer: 1500
            })
            history.push("/presensi");
            // setTimeout(() => {
            //   window.location.reload();
            // }, 1000);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
            console.log(error);
          });
      };


  return (
    <div>
         <div className="edit">
        <div className="edit mx-5">
          <div className="container my-5 edit1">
            <Form onSubmit={putAbsen}>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Username</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="text"
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Gender</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="text"
                    value={gender}
                    onChange={(e) => setgender(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="birth-date mb-3">
                <Form.Label>
                  <strong>Tanggal</strong>
                </Form.Label>
                <div className="d-flex gap-3">
                  <Form.Control
                    type="date"
                    value={tanggal}
                    onChange={(e) => setTanggal(e.target.value)}
                    required
                  />
                </div>
              </div>
              <div className="d-flex justify-content-end align-item-center mt-2">
                <button className="buton btn" type="submit">
                  Save
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  )
}
