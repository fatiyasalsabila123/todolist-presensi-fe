import React from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import Logo from "../image/maxresdefault-removebg-preview.png"

export default function Navbar() {
  const history = useHistory();

  const logout = () => {
    Swal.fire({
      title: "Anda Yakin Ingin Logout",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout!!!",
          showConfirmButton: false,
          timer: 2500,
        });
      }
      setTimeout(()=>{
        
      window.location.reload();
      }, 1500)
    });
    localStorage.clear();
    //Untuk munuju page selanjutnya
    history.push("/");
  };
  return (
    <div>
      <nav className="bg-blue-500 ">
        <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl px-4 md:px-6 py-2.5">
          <div className="flex items-center">
            <a href="/home"><img
              src={Logo}
              className="h-6 mr-3 sm:h-9"
              alt=""
            /></a>
            <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
              Presensi & To Do List
            </span>
          </div>
          {localStorage.getItem("id") !== null ? (
            <>
              <div className="flex items-center">
                <button
                  className="text-sm font-medium  dark:text-blue-500 hover:underline text-white"
                  onClick={logout}
                >
                 <i className="fa-solid fa-right-from-bracket text-white"></i> LOGOUT
                </button>
              </div>
            </>
          ) : (
            <li className="nav-item"></li>
          )}
        </div>
      </nav>
      <nav className="bg-gray-200">
        <div className="max-w-screen-xl px-4 py-3 mx-auto md:px-6">
          <div className="flex items-center">
            <ul className="flex flex-row mt-0 mr-6 space-x-8 text-sm font-medium">
              <li>
                <a
                  href="/profile"
                  className="text-gray-900 dark:text-white hover:underline"
                  aria-current="page"
                >
                  Profile
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
