import React from "react";
import Logo from "../image/maxresdefault-removebg-preview.png"

export default function Footer() {
  return (
    <div>
      <footer className="p-4 rounded-lg md:px-6 md:py-8 bg-blue-500 ">
        <div className="sm:flex sm:items-center sm:justify-between">
          <div
            className="flex items-center mb-4 sm:mb-0"
          >
            <img
              src={Logo}
              className="h-8 mr-3"
              alt=""
            />
            <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
              Fatiya Salsabila
            </span>
          </div>
          <ul className="flex flex-wrap items-center mb-6 text-sm text-black sm:mb-0 dark:text-gray-400">
            <li >
              <a href="https://gitlab.com/fatiyasalsabila123/todolist-presensi-fe.git" className="mr-4 hover:underline md:mr-6 ">
              <i className="fab fa-gitlab"></i>
              </a>
            </li>
            <li>
              <a href="https://mail.google.com/mail/u/0/#inbox" className="mr-4 hover:underline md:mr-6">
              <i className="fas fa-envelope"></i>
              </a>
            </li>
            <li>
              <a href="https://www.instagram.com" className="mr-4 hover:underline md:mr-6 ">
              <i className="fab fa-instagram-square"></i>
              </a>
            </li>
            <li>
              <a href="https://twitter.com" className="hover:underline">
              <i className="fab fa-twitter"></i>
              </a>
            </li>
          </ul>
        </div>
        <hr className="my-6 border-black sm:mx-auto dark:border-gray-500 lg:my-8" />
        <span className="block text-sm text-black sm:text-center dark:text-gray-400">
           {" "}
          <p className="hover:underline">
          <i className="fas fa-copyright"></i> fatiya salsabila
          </p>
          . All Rights Reserved.
        </span>
      </footer>
    </div>
  );
}
