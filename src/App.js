// import logo from "./logo.svg";
import { BrowserRouter, Route, Switch } from "react-router-dom";
// import TodoForm from "./component/TodoForm";
// import { useState } from "react";
// import Header from "./component/Header";
// import TodoList from "./component/TodoList";
import "../src/App.css"
import Register from "./page/Register";
// import Home from "./component/Home";
import Login from "./page/Login";
// import Home1 from "./component/Home1";
import Dashboard from "./component/Dashboard";
import Home1 from "./component/Home1";
import Presensi from "./page/Presensi";
import Todo from "./component/Todo";
import Profile from "./page/Profile";

function App() {
  // const [input, setInput] = useState("");
  // const [todos, setTodos] = useState([]);
  // const [editTodo, setEditTodo] = useState(null);
  return (
    <div className="app">
      {/* <div className="app-wrapper">
        <div>
          <Header />
        </div>
        <div>
          <TodoForm
            input={input}
            setInput={setInput}
            todos={todos}
            setTodos={setTodos}
            editTodo={editTodo}
            setEditTodo={setEditTodo}
          />
        </div>
        <div>
          <TodoList
            todos={todos}
            setTodos={setTodos}
            setEditTodo={setEditTodo}
          />
        </div>
      </div> */}
      <BrowserRouter>
      <main>
        <Switch>
      {/* <Route path="/home" component={Home} exact /> */}
      <Route path="/register" component={Register} exact />
      <Route path="/" component={Login} exact />
      <Route path="/todolist" component={Dashboard} exact />                                                                    
      <Route path="/home" component={Home1} exact />
      <Route path="/presensi" component={Presensi} exact />
      <Route path="/profile" component={Profile} exact/>
      </Switch>
      </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
